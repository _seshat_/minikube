### [1]
- install minikube : https://kubernetes.io/docs/tasks/tools/install-minikube/

#### [1.1] - installation minikube
https://gitlab.com/_seshat_/minikube

- install kubectl
- install minikube
- start minikube : minikube start --vm-driver=none
- minikube status

#### [1.2] - creation et upload d'une image vers hub.docker
- cloner le repo https://gitlab.com/_seshat_/front
- creer un dockerfile pour l'application vuejs front clonee precedemment
  - FROM node:latest
- creer un compte sur https://hub.docker.com/signup?next=%2F%3Fref%3Dlogin
- sudo apt install gnupg2 pass
- sudo docker login
- upload image sur hub.docker : docker push <your-tag>

### [2]
- from : https://www.linode.com/docs/kubernetes/deploy-container-image-to-kubernetes/

#### [2.1] create namespace for vuejs-site
- create vuejs-pod directory : mkdir -p ~/k8s-vuejs/
- create the manifest file for your vuejs site’s namespace
    - create file : ns-vuejs-site.yml
        - The manifest file declares the version of the API in use, the kind of resource that is being defined, and metadata about the resource. All manifest files should provide this information.    
```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: vuejs-site
```
- The key-value pair name: vuejs-site defines the namespace object’s unique name.
- use the manifest to create the namespace with : kubectl create -f ~/k8s-vuejs/ns-vuejs-site.yaml
- verify if the namespace was added : kubectl get namespaces

#### [2.2] create the service for vuejs-site
- The service will group together all pods for the VueJS site, expose the same port on all pods to the internet, and load balance site traffic between all pods. It is best to create a service prior to any controllers (like a deployment) so that the Kubernetes scheduler can distribute the pods for the service as they are created by the controller.

The Vuejs site’s service manifest file will use the NodePort method to get external traffic to the Vuejs site service. NodePort opens a specific port on all the Nodes and any traffic that is sent to this port is forwarded to the service. Kubernetes will choose the port to open on the nodes if you do not provide one in your service manifest file. It is recommended to let Kubernetes handle the assignment. Kubernetes will choose a port in the default range, 30000-32767.
- Create the manifest file for your service : service-vuejs.yml
```yaml
apiVersion: v1
kind: Service
metadata:
  name: vuejs-site
  namespace: vuejs-site
spec:
  selector:
    app: vuejs-site
  ports:
  - protocol: TCP
    port: 8080
    targetPort: 8080
  type: NodePort
```
- The spec key defines the Vuejs site service object’s desired behavior. It will create a service that exposes TCP port 80 on any pod with the app: vuejs-site label.
- The exposed container port is defined by the targetPort:80 key-value pair.
- use the manifest to create the service : kubectl create -f ~/k8s-vuejs/service-vuejs.yml
- check if the service was correctly declared : kubectl get services -n vuejs-site

#### [2.3] create the deployment for vuejs-site
- A deployment is a controller that helps manage the state of your pods. The Vuejs site deployment will define how many pods should be kept up and running with the Vuejs site service and which container image should be used.
- Create the manifest file for your Vuejs site’s deployment.
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: vuejs-site
  namespace: vuejs-site
spec:
  replicas: 2
  selector:
    matchLabels:
      app: vuejs-site
  template:
    metadata:
      labels:
        app: vuejs-site
    spec:
      containers:
      - name: vuejs-site
        image: aendlyx/vuejs-site
        imagePullPolicy: Always
        ports:
        - containerPort: 8080
```
- The deployment’s object spec states that the deployment should have 3 replica pods. This means at any given time the cluster will have 3 pods that run the Vuejs site service.
- The template field provides all the information needed to create actual pods.
- The label app: Vuejs-site helps the deployment know which service pods to target.
- The container field states that any containers connected to this deployment should use the Vuejs site image aendlyx/vuejs-site that was created in the Build the Docker Image section of this guide.
- imagePullPolicy: Always means that the container image will be pulled every time the pod is started.
- containerPort: 80 states the port number to expose on the pod’s IP address. - The system does not rely on this field to expose the container port, instead, it provides information about the network connections a container uses.
- Create the deployment for your Vuejs site : kubectl create -f ~/k8s-vuejs/deployment.yml
- View the vuejs site deployment : kubectl get deployment vuejs-site -n vuejs-site

### [2.4] Acces Vuejs App
- show url of vuejs app : sudo minikube service vuejs-site --url -n vuejs-site    
  - ex : http://10.0.2.15:30485
- add port redirection in vbox
- access your app on 127.0.0.1:30485

### [3.1] Acces Kubernetes Dashboard
- launch the kubernetes dashboard : minikube dashboard
- authorise external connection on the dashboard : kubectl proxy --address='0.0.0.0' --disable-filter=true 
- on a browser : http://127.0.0.1:8001/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/p
roxy/
